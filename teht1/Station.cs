namespace teht1
{
    public class Station
    {
        public int id {get; set; }
        public string name {get; set;}
        public double x {get; set; }
        public double y {get; set; }
        public int bikesAvailable {get; set;}
        public int spacesAvailable {get; set;}
        public bool allowDropOff {get; set;}

    }
}