using System.Threading.Tasks;

namespace teht1
{

    public interface ICityBikeDataFetcher
    {
        

         Task<int> GetBikeCountInStation(string stationName);
    }
}