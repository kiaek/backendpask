using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;

namespace teht1
{
    public class RealTimeCityBikeFetcher : ICityBikeDataFetcher
    {
        //public Station [] stations {get; set;}

        public async Task<int> GetBikeCountInStation(string stationName)
        {

            int bikeCountInStation = -1;
            HttpResponseMessage response = await Program.client.GetAsync("http://api.digitransit.fi/routing/v1/routers/hsl/bike_rental");
            if (response.IsSuccessStatusCode)
            {
                byte [] bytes = response.Content.ReadAsByteArrayAsync().Result;
                string data = System.Text.Encoding.UTF8.GetString(bytes);
                Data stationData = Newtonsoft.Json.JsonConvert.DeserializeObject<Data>(data);

                foreach(Station station in stationData.stations)
                {
                    if (station.name == stationName)
                    {
                        bikeCountInStation = station.bikesAvailable;
                    }
                    
                }
            
                //Console.WriteLine(data);
            }
            return bikeCountInStation;
        
            //throw new System.NotImplementedException();
        }
        
    }
}