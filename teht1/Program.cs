﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;

namespace teht1
{
    class Program
    {
        public static HttpClient client = new HttpClient();
        public static RealTimeCityBikeFetcher onlineBikeDataFetcher = new RealTimeCityBikeFetcher();
        public static OfflineCityBikeDataFetcher offlineCityBikeDataFetcher = new OfflineCityBikeDataFetcher();


        public static void FetchBikeData(string stationName, bool realTime)
        {          
            Task<int> task;

            try 
            {
                ValidStationName(stationName);  

                if (realTime)
                {
                    task = onlineBikeDataFetcher.GetBikeCountInStation(stationName);
                }
                else
                {
                    task = offlineCityBikeDataFetcher.GetBikeCountInStation(stationName);
                }

                task.Wait();
                StationNameNotFound(task.Result);
                Console.WriteLine(task.Result);  
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Error in station name: " + e.Message);
            }
            catch (NotFoundException e)
            {
                Console.WriteLine("Error in station name: " + e.Message);
            }
        }

        public static void ValidStationName(string stationName)
        {
            if (stationName.Any(char.IsDigit))
            {
                throw new System.ArgumentException("Parameter cannot be digit");
            }
        }
        public static void StationNameNotFound(int returnValue)
        {
            if (returnValue == -1)
            {
                throw new NotFoundException("Station not found");
            }
        }
        static void Main(string[] args)
        {
            string input = "";
            string state = "";
            
            while (input != "exit" || state != "exit")
            {
                Console.WriteLine("Type 'exit' to quit \n");

                Console.WriteLine("Enter bike station to be searched:");
                input = Console.ReadLine();
            
                Console.WriteLine("Would you like to read online or offline?\n");
                state = Console.ReadLine();
                if (state == "online" || state == "Online")
                {
                   FetchBikeData(input, true);
                }
                else if (state == "offline" || state == "Offline")
                {
                    FetchBikeData(input, false);
                }
            }
        }
    }
}
