using System.Threading.Tasks;
using System;
using System.IO;

namespace teht1
{
    public class OfflineCityBikeDataFetcher : ICityBikeDataFetcher
    {
        public async Task<int> GetBikeCountInStation(string stationName)
        {
            int bikeCountInStation = -1;
            try
            {
                using (StreamReader sr = new StreamReader("bikes.txt"))
                {
                    int counter = 0;
                    string[] data = File.ReadAllLines("bikes.txt");
                    string line = data[counter];

                    while ((line = (sr.ReadLine())) != null)
                    {
                        if(line.Contains(stationName))
                        {
                            string result = System.Text.RegularExpressions.Regex.Match(line, @"\d+").Value;
                            bikeCountInStation = Int32.Parse(result);
                        }
                        counter++;
                    }
                    /* 
                    string data = await sr.ReadToEndAsync();
                    Data stationData = Newtonsoft.Json.JsonConvert.DeserializeObject<Data>(data);

                    foreach(Station station in stationData.stations)
                    {
                        if (station.name == stationName)
                        {
                            bikeCountInStation = station.bikesAvailable;
                        }
                    
                    }
                    */
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:" + e.Message);

            }
            return bikeCountInStation;

        }
    }
}