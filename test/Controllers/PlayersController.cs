using Microsoft.AspNetCore.Mvc;

namespace backendpask.test.controllers

{

    [Route("api/players")]

    public class PlayersController
    {
        //Create, read, update, delete
        //http pyyntöjä ja controlleri keskustelee processorin kanssa, ja prosessori repon kanssa
        //repo -> prosessori -> controlleri -> käyttäjä
        //attribuutit tänne

        //vastaanottaa käyttäjän pyynnöt -> välittää ne prosessorille

        private PlayersProcessor _playersProcessor;

        public PlayersController(PlayersProcessor playersProcessor)
        {
            _playersProcessor = playersProcessor;
        }

        [HttpPost]
        public async Task<NewPlayer> Create([FromBody]NewPlayer player)
        {
            return await _playersProcessor.Create(player);
        }

        [HttpDelete("{id:Gui}")]
        public async Task<Player> Delete(Guid id)
        {
            return await _playersProcessor.Delete(id);
        }

        [HttpGet]
        public async Task<Player> GetAll()
        {
            return await _playersProcessor.GetAll();
        }

        [HttpGet("{id:Gui}")]
        public async Task<Player> Get(Guid id)
        {
            return await _playersProcessor.Get(id);
        }

        [HttpPost("{id:Gui}")]
        public async Task<Player> Modify(Guid id, [FromBody]ModifiedPlayer player)
        {
            return await _playersProcessor.Modify(id, player);
        }

    }
}