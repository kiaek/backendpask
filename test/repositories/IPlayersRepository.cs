using System;
using System.Threading.Tasks;
using backendpask.teht2.models;

namespace backendpask.teht2.repositories
{
    public interface IPlayersRepository
    {
        Task<Player> Create(Player player);

        //Task<Player> Read();

        //Task<Player> Update();

        Task<Player> Delete(Guid id);

        Task<Player[]> GetAll();

        Task<Player> Get(Guid id);

        Task<Player> Modify(Guid id, Player player);
        
    };
}