using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using backendpask.teht2.models;
using System.Linq;

namespace backendpask.teht2.repositories
{
    public class PlayersInMemoryRepository : IPlayersRepository
    {
        Dictionary <Guid, Player> _players = new Dictionary <Guid, Player>();

        public PlayersInMemoryRepository()
        {
            
        }

        public async Task<Player> Create(Player player)
        {
            _players.Add(player.id, player);
            return player;
        }

        public async Task<Player> Delete(Guid id)
        {
            Player deletedPlayer = _players[id];
            _players.Remove(id);
            return deletedPlayer;         
        }

        public async Task<Player[]>GetAll()
        {
            return _players.Values.ToArray();
        }

        public async Task<Player> Get(Guid id)
        {
            return _players[id];
        }

        public async Task<Player> Modify(Guid id, Player player)
        {
            _players[id] = player;
            return player;
        }
    }
}