using System.Threading.Tasks;
using backendpask.teht2.models;
using backendpask.teht2.repositories;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace backendpask.teht2.processors
{
    public class PlayersProcessor
    {
        //kaikki laskeminen tapahtuu täällä
        //täällä haetaan reposta tietoa
        //tieto välitetään täältä controllerille, joka välittää sen käyttäjälle


        private IPlayersRepository _repository;

        

        public PlayersProcessor(IPlayersRepository repository)
        {
            _repository = repository;
        }

        public Task<Player> Create(NewPlayer player)
        {
            Player _player = new Player() 
            {
                id = Guid.NewGuid(),
                name = player.name,                
                level = 1
            };
            return _repository.Create(_player);
        }

        public Task<Player> Delete(Guid id)
        {
            return _repository.Delete(id);
        }

        public Task<Player[]> GetAll()
        {
            return _repository.GetAll();
        }

        public Task<Player> Get(Guid id)
        {
            return _repository.Get(id);
        }

        public Task<Player> Modify(Guid id, ModifiedPlayer player)
        {
            Player tempPlayer = _repository.Get(id).Result;
            tempPlayer.name = player.name;
            return _repository.Modify(id, tempPlayer);
        } 
    }
}