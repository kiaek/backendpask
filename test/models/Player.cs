using System;

namespace backendpask.teht2.models
{
    public class Player
    {
        public Guid id {get; set;}
        public string name {get; set;}
        public int level {get; set;}

    }
}