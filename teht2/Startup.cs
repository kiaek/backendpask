﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using teht2.processors;
using teht2.repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using teht2.Middleware;
using teht2.Mongodb;
using Microsoft.AspNetCore.Authentication;
using teht2.models;

namespace backendpask
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AuthKey>(Configuration);

            services.AddMvc();
            services.AddSingleton<PlayersProcessor>();
           // services.AddSingleton<IPlayersRepository, PlayersInMemoryRepository>();
            services.AddSingleton<ItemProcessor>();
            services.AddSingleton<MongoDBClient>();
            services.AddSingleton<IRepository, MongoDbRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMiddleware<ErrorHandlingMiddleware>();
            app.UseMiddleware<AuthMiddleware>();
            app.UseMvc();
        }
    }
}
