using System;
using System.ComponentModel.DataAnnotations;

namespace teht2.models
{
    public class Item
    {
        public Guid id {get; set;}

        public string name {get; set;}

        [Range(1, 100)]
        public int level {get; set;}

        public int price {get; set;}

        public string type {get; set;}

        public DateTime creationDate {get; set;}
    }
}