using System;
using System.Collections.Generic;

namespace teht2.models
{
    public class Player
    {
        public Guid id {get; set;}
        public string name {get; set;}
        public int level {get; set;}
        public int score {get; set;}
        
        //private Dictionary<Guid, Item> items = new Dictionary<Guid, Item>();

        public Item[] items;

    }
}