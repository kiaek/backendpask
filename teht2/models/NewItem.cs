using System;
using System.ComponentModel.DataAnnotations;
using teht2.ModelValidation;

namespace teht2.models
{
    public class NewItem
    {
        [Required]
        public string name {get; set;}

        [Range(1, 100)]
        public int level {get; set;}

        public int price {get; set;}

        [MinimumLevelValidation]
        public string type {get; set;}

        public DateTime creationDate {get; set;}
    }
}