using System.ComponentModel.DataAnnotations;
using teht2.models;

namespace teht2.ModelValidation
{
    public class MinimumLevelValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            NewItem item = (NewItem)validationContext.ObjectInstance;
            
            if(item.type.Equals("sword") && item.level < 8)
            {
                return new ValidationResult("The item level of type 'sword' has a minimum equip level of 8");
            }
            return ValidationResult.Success;
        }
    }
}