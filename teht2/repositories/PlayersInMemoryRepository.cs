using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using teht2.exceptions;
using teht2.models;

namespace teht2.repositories
{
    /*
    public class PlayersInMemoryRepository : IPlayersRepository
    {
        
        Dictionary <Guid, Player> _players = new Dictionary <Guid, Player>();

        public PlayersInMemoryRepository()
        {
            
        }

        public async Task<Player> Create(Player player)
        {
            _players.Add(player.id, player);
            return player;
        }

        public async Task<Player> Delete(Guid id)
        {
            Player deletedPlayer = _players[id];
            _players.Remove(id);
            return deletedPlayer;         
        }

        public async Task<Player[]>GetAll()
        {
            return _players.Values.ToArray();
        }

        public async Task<Player> Get(Guid id)
        {
            //return _players[id];
            if(_players.ContainsKey(id) == false)
            {
                throw new NotFoundException();
            }
            return _players[id];
        }

        public async Task<Player> Modify(Guid id, Player player)
        {
            _players[id] = player;
            return player;
        }

        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            return _players[playerId].GetItem(itemId);
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            return _players[playerId].GetItems().Values.ToArray();
        }

        public async Task<Item> CreateItem(Guid playerId, Item item)
        {
            if (_players[playerId].level < item.level)
            {
                throw new NotHighEnoughLevelException();
            }
            _players[playerId].AddItem(item.id, item);
            return item;
        }

        public async Task<Item> ModifyItem(Guid playerId, Guid itemId, Item item)
        {
            _players[playerId].GetItems()[itemId] = item;
            return item;
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            Item deletedItem = _players[playerId].GetItem(itemId);
            _players[playerId].GetItems().Remove(itemId);
            return deletedItem;
        }
        
    }
    */
}