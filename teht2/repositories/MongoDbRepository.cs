using System;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using teht2.models;
using teht2.Mongodb;
using System.Linq;
using System.Collections.Generic;

namespace teht2.repositories
{
    public class MongoDbRepository : IRepository
    {
        private IMongoCollection<Player> _collection; 

        public MongoDbRepository(MongoDBClient client)
        {
            IMongoDatabase database = client.GetDatabase("game");
            _collection = database.GetCollection<Player>("players");
        }

        public async Task<Item> CreateItem(Guid playerId, Item item)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.id, playerId);
            var update = Builders<Player>.Update.Push(p => p.items, item);
            await _collection.FindOneAndUpdateAsync(filter, update);
            return item;
        }

        public async Task<Player> CreatePlayer(Player player)
        {
            await _collection.InsertOneAsync(player);
            return player;
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid itemId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.id, playerId);
            var update = Builders<Player>.Update.PullFilter(p => p.items, Builders<Item>.Filter.Eq(i => i.id, itemId));
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();     

            Item[] tempItems = player.items;
            Item tempItem = null;
            
            for(int i = 0; i < tempItems.Length; i++)
            {
                if(tempItems[i].id.Equals(itemId))
                {
                    tempItem = tempItems[i];
                    break;
                }
            }
            tempItems = tempItems.Where(i => !(i.id.Equals(itemId))).ToArray();
            player.items = tempItems;
            await _collection.ReplaceOneAsync(filter, player);

            return tempItem;
            //vois tehä exceptionin ehkä
        }

        public async Task<Player> DeletePlayer(Guid playerId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();

            await _collection.DeleteOneAsync(filter);
            return player;
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();
            return player.items;    
        }

        public async Task<Player[]> GetAllPlayers()
        {
            var list = await _collection.Find(_ => true).ToListAsync();
            var players = list.ToArray();
            return players;
        }

        public async Task<Player[]> GetAllByInventory(int size)
        {
            var filter = Builders<Player>.Filter.Size(p => p.items, size);
            var list = await _collection.Find(filter).ToListAsync();
            return list.ToArray();
        }

        public async Task<int> GetCommonPlayerLevel()
        {
            var aggregate = _collection.Aggregate()
            .Project(new BsonDocument { {"level", 1 } })
            .Group(new BsonDocument { { "id", "&level" }, {"Count", new BsonDocument("$sum", 1)}})
            .Sort(new BsonDocument { { "Count", -1 } } )
            .Limit(1);

            BsonDocument result = await aggregate.FirstAsync();
            return result["level"].ToInt32();
        }

        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();

            Item[] tempItems = player.items;
            for(int i = 0; i < tempItems.Length; i++)
            {
                if(tempItems[i].id.Equals(itemId))
                {
                    return tempItems[i];
                }
            }
            //vois tehä exceptionin ehkä
            return null;
        }

        public async Task<Player> GetPlayer(Guid playerId)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.id, playerId);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();
            return player;
        }

        public async Task<Player> GetPlayer(string name)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.name, name);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();
            return player;
        }

        public async Task<Player[]> GetPlayersWithScoreMoreThan(int playerScore)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.score, playerScore);
            //var cursor = await _collection.FindAsync(filter);
            var list = await _collection.Find(filter).ToListAsync();

            List<Player> playerList = new List<Player>();

            for(int i = 0; i < list.Count; i++)
            {
                if(list[i].score > playerScore)
                {
                    playerList.Add(list[i]);
                }
            }
            //vois tehä exceptionin ehkä
            
            return playerList.ToArray();
        }

        public async Task<Item> UpdateItem(Guid playerId, Item item)
        {
            var filter = Builders<Player>.Filter.And(Builders<Player>.Filter.Eq(p => p.id, playerId),
            Builders<Player>.Filter.ElemMatch(p => p.items, i => i.id == item.id));
            
            var update = Builders<Player>.Update.Set(p => p.items[-1], item);
            
            await _collection.FindOneAndUpdateAsync(filter, update);
            return item;
        }

        public async Task<Player> UpdatePlayer(Player player)
        {
            var filter =  Builders<Player>.Filter.Eq(p => p.id, player.id);
            await _collection.ReplaceOneAsync(filter, player);

            return player;
        }

           //alt shift f = koodin formatointi 
    }
}