using System;
using System.Threading.Tasks;
using teht2.models;

namespace teht2.repositories
{
    public interface IRepository
    {
        Task<Player> CreatePlayer(Player player);
        Task<Player> GetPlayer(Guid playerId);
        Task<Player> GetPlayer(string name);
        Task<Player[]> GetAllPlayers();
        Task<Player[]> GetAllByInventory(int size);
        Task<int> GetCommonPlayerLevel();
        Task<Player[]> GetPlayersWithScoreMoreThan(int playerScore);
        Task<Player> UpdatePlayer(Player player);
        Task<Player> DeletePlayer(Guid playerId);
        Task<Item> CreateItem(Guid playerId, Item item);
        Task<Item> GetItem(Guid playerId, Guid itemId);
        Task<Item[]> GetAllItems(Guid playerId);
        Task<Item> UpdateItem(Guid playerId, Item item);
        Task<Item> DeleteItem(Guid playerId, Guid item);
    }
}