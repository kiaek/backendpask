using System;
using System.Threading.Tasks;
using teht2.models;
using teht2.repositories;

namespace teht2.processors
{
    public class ItemProcessor
    {
        private IRepository _repository;
        
        public ItemProcessor(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            return await _repository.GetItem(playerId, itemId);
        }

        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            return await _repository.GetAllItems(playerId);
        }

        public async Task<Item> CreateItem(Guid playerId, NewItem item)
        {
            Random rand = new Random();

            Item _item = new Item()
            {
                id = Guid.NewGuid(),
                name = item.name,
                level = item.level,
                price = rand.Next(1, 500),
                type = item.type,
                creationDate = DateTime.Today
            };
            return await _repository.CreateItem(playerId, _item);
        }

        public async Task<Item> UpdateItem(Guid playerId, Guid itemId, ModifiedItem item)
        {
            Item _item = _repository.GetItem(playerId, itemId).Result;
            _item.name = item.name;
            return await _repository.UpdateItem(playerId, _item);
        }

        public async Task<Item> DeleteItem(Guid playerId, Guid item)
        {   
            return await _repository.DeleteItem(playerId, item);
        }
        
    }
}