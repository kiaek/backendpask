using System.Threading.Tasks;
using teht2.repositories;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using teht2.models;

namespace teht2.processors
{
    public class PlayersProcessor
    {
        //kaikki laskeminen tapahtuu täällä
        //täällä haetaan reposta tietoa
        //tieto välitetään täältä controllerille, joka välittää sen käyttäjälle


        private IRepository _repository;

        

        public PlayersProcessor(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<Player> Create(NewPlayer player)
        {
            Player _player = new Player() 
            {
                id = Guid.NewGuid(),
                name = player.name,                
                level = 1,
                score = 0
            };
            return await _repository.CreatePlayer(_player);
        }

        public async Task<Player> Delete(Guid id)
        {
            return await _repository.DeletePlayer(id);
        }

        public async Task<Player[]> GetAll()
        {
            return await _repository.GetAllPlayers();
        }

        public async Task<Player[]> GetAllByInventory(int size)
        {
            return await _repository.GetAllByInventory(size);
        }

        public async Task<int> GetCommonPlayerLevel()
        {
            return await _repository.GetCommonPlayerLevel();
        }

        public async Task<Player> Get(Guid id)
        {
            return await _repository.GetPlayer(id);
        }

        public async Task<Player> Get(string name)
        {
            return await _repository.GetPlayer(name);
        }

        public async Task<Player[]> GetPlayersWithScoreMoreThan(int score)
        {
            return await _repository.GetPlayersWithScoreMoreThan(score);
        }

        public async Task<Player> UpdatePlayer(Guid id, ModifiedPlayer player)
        {
            Player tempPlayer = await _repository.GetPlayer(id);
            tempPlayer.name = player.name;
            return await _repository.UpdatePlayer(tempPlayer);
        } 
    }
}