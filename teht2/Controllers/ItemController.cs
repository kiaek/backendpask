using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using teht2.models;
using teht2.ModelValidation;
using teht2.processors;

namespace teht2.Controllers
{

    [Route("api/players/{playerid}/items")]
    public class ItemController
    {
        private ItemProcessor _itemprocessor;
        
        public ItemController(ItemProcessor itemprocessor)
        {
            _itemprocessor = itemprocessor;
        }

        [HttpGet("{itemId:Guid}")]
        public async Task<Item> GetItem(Guid playerId, Guid itemId)
        {
            return await _itemprocessor.GetItem(playerId, itemId);
        }

        [HttpGet]
        public async Task<Item[]> GetAllItems(Guid playerId)
        {
            return await _itemprocessor.GetAllItems(playerId);           
        
        }

        [ValidateModel]
        [HttpPost]
        public async Task<Item> CreateItem(Guid playerId, [FromBody]NewItem item)
        {
            return await _itemprocessor.CreateItem(playerId, item);
        }

        [HttpPost("{itemId:Guid}")]
        public async Task<Item> UpdateItem(Guid playerId, Guid itemId, [FromBody]ModifiedItem item)
        {
            return await _itemprocessor.UpdateItem(playerId, itemId, item);
        }

        [HttpDelete("{itemId:Guid}")]
        public async Task<Item> DeleteItem(Guid playerId, Guid item)
        {
            return await _itemprocessor.DeleteItem(playerId, item);
        }
    }
}