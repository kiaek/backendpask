using System;
using System.Threading.Tasks;
using teht2.models;
using teht2.processors;
using Microsoft.AspNetCore.Mvc;
using teht2.ModelValidation;

namespace teht2.Controllers

{

    [Route("api/players")]

    public class PlayersController : Controller
    {
        //Create, read, update, delete
        //http pyyntöjä ja controlleri keskustelee processorin kanssa, ja prosessori repon kanssa
        //repo -> prosessori -> controlleri -> käyttäjä
        //attribuutit tänne

        //vastaanottaa käyttäjän pyynnöt -> välittää ne prosessorille

        private PlayersProcessor _playersProcessor;

        public PlayersController(PlayersProcessor playersProcessor)
        {
            _playersProcessor = playersProcessor;
        }

        [HttpPost]
        [ValidateModel]
        public async Task<Player> Create([FromBody]NewPlayer player)
        {
            return await _playersProcessor.Create(player);
        }

        [HttpDelete("{id:Guid}")]
        public async Task<Player> Delete(Guid id)
        {
            return await _playersProcessor.Delete(id);
        }

        [HttpGet]
        [HttpGet("{size:int}")]
        public async Task<Player[]> GetAll(int? size)
        {
            if (size != null)
            {
                return await _playersProcessor.GetAllByInventory((int)size);
            }

            return await _playersProcessor.GetAll();
        }

        [HttpGet("{commonlevel}")]
        public async Task<int> GetCommonPlayerLevel()
        {
            return await _playersProcessor.GetCommonPlayerLevel();
        }

        [HttpGet("{id:Guid}")]
        [HttpGet("{name}")]
        public async Task<Player> Get(Guid id, string name)
        {
            if(name != null)
            {
                return await _playersProcessor.Get(name);
            }
            return await _playersProcessor.Get(id);
        }

        [HttpGet("{score:int}")]
         public async Task<Player[]> GetPlayersWithScoreMoreThan(int score)
        {
            return await _playersProcessor.GetPlayersWithScoreMoreThan(score);
        }


        [HttpPost("{id:Guid}")]
        public async Task<Player> UpdatePlayer(Guid id, [FromBody]ModifiedPlayer player)
        {
            return await _playersProcessor.UpdatePlayer(id, player);
        }

    }
}