using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using teht2.exceptions;

namespace teht2.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            bool failed = false;
            bool notHighEnoughLevel = false;

            try
            {
                await _next(context);
            }
            catch(NotFoundException e)
            {
                context.Response.StatusCode = 404;
                failed = true;
            }
            catch(NotHighEnoughLevelException e)
            {
                //not accepted
                context.Response.StatusCode = 406;
                notHighEnoughLevel = true;
            }

            if (failed)
            {
                await context.Response.WriteAsync("Could not find player");
            }

            if(notHighEnoughLevel)
            {
                await context.Response.WriteAsync("Player level not high enough for this item");
            }

        }
        
    }
}