using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using teht2.models;

namespace teht2.Middleware
{
    public class AuthMiddleware
    {
        private readonly AuthKey _authKey;
        private RequestDelegate _next;
        public AuthMiddleware(IOptions<AuthKey> authKey, RequestDelegate next)
        {
            _authKey = authKey.Value;
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            string apikey = _authKey.ApiKey;
            StringValues values;

            bool apiKeyExists = context.Request.Headers.TryGetValue("API_KEY", out values);
            bool authorized = false;            

            if (apiKeyExists)
            {
                string[] keys = values.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    if(keys[i].Equals(apikey))
                    {
                        authorized = true;
                    }
                }
            }

            if (authorized)
            {
                await _next(context);
            }
            else if (!apiKeyExists)
            {
                //Bad request
                context.Response.StatusCode = 400;
            }
            else
            {
                //unauthorized key value
                context.Response.StatusCode = 401;
            }

        }
        


    }
}